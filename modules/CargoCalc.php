<?php

namespace modules;

class CargoCalc
{

    private $NONCE,
            $DIR_PATCH,
            $DATA_LIST,
            $IS_TERMO,
            $GET_VOLUME,
            $GET_WEIGHT,
            $VOLUME_WEIGHT,
            $FINDED_DIRECTION,
            $OPTION_NAME,
            $RES_TOTAL_PRICE,
            $RES_VOLUME,
            $RES_WEIGHT,
            $RES_PALLETS,
            $HALF_PALET,
            $PRICE,
            $HALF_PALET_WEIGHT,
            $HALF_PALET_VOLUME,
            $RES_PALLETS_WEIGHT,
            $RES_PALLETS_VOLUME,
            $FP_WEIGHT_NORM,
            $HP_WEIGHT_NORM,
            $FP_VOLUME_NORM,
            $HP_VOLUME_NORM;

    public function __construct() {

        $this->NONCE = 'CargoCalc';


        add_action('wp_ajax_calc-get-palces', array($this, 'ajaxGetPlacesWhere'));
        add_action('wp_ajax_nopriv_calc-get-palces', array($this, 'ajaxGetPlacesWhere'));

        add_action('wp_ajax_calc-result', array($this, 'calcAjaxGetResult'));
        add_action('wp_ajax_nopriv_calc-result', array($this, 'calcAjaxGetResult'));

        if (!is_admin())
            add_shortcode('cargo-calc', array($this, 'cargoCalcShortcode'));
    }

    /**
     * Передаем данные в фронт скрипт калькулятора
     */
    public function frontEndLocalizeScript() {
        wp_localize_script('cargo-calc-js', 'ajaxFrontData', array(
            'ajax' => admin_url('admin-ajax.php'),
            'nonce' => wp_create_nonce($this->NONCE)
        ));
    }

    /*
     * setOptionName
     */

    public function setOptionName($ption_name = '') {
        if (empty($ption_name))
            return false;

        $this->OPTION_NAME = $ption_name;
        return true;
    }

    /*
     * setBaseDirPath
     */

    public function setBaseDirPath($baseDirPath = '') {
        if (empty($baseDirPath))
            return false;

        $this->DIR_PATCH = $baseDirPath;
        return true;
    }

    /*
     * getDataList
     */

    public function getDataList($isTermo) {

        $termo = $isTermo ? 'isTermo' : 'noTermo';

        if (!is_array($this->DATA_LIST)) {

            $data_list = unserialize(get_option($this->OPTION_NAME));
            $this->DATA_LIST = $data_list;

            return $data_list[$termo];
        } else {
            $data_list = $this->DATA_LIST;
            return $data_list[$termo];
        }
    }

    public function autocompliteFunc($temp='', $inputValue = '', $direction = '') {
        $deliveryPoint = false;

        if (!empty($temp)) {
            if (empty($inputValue)) {

                $deliveryPoint = $direction; // ['delivery_to']
            } else {
                if (stristr($temp, $inputValue))
                    $deliveryPoint = $direction;
            }
        }
        return $deliveryPoint;
    }

    /**
     * Функция отдает список городов-отправителей на основе принятых начальных букв названия
     * @return void
     */
    public function ajaxGetPlacesWhere() {

        check_ajax_referer($this->NONCE, 'nonce');

        $this->IS_TERMO = $_REQUEST['isTermo'] == "true" ? true : false;


        $inpytValue = $_REQUEST['value'];       //Значение в поле
        $from = $_REQUEST['from'];              //значение поля откуда везем
        $type = $_REQUEST['type'];          // тип заполняемого поля 'from' или 'to'

        
        
        $data_list = $this->getDataList($this->IS_TERMO);

        $list = array();
        foreach ($data_list as $direction) {
            
            $inputValueL = mb_strtolower ($inpytValue);
            
            if ($type == 'from') {
                
                $tempL = mb_strtolower ($direction[0]);

                $deliveryPoint = $this->autocompliteFunc($tempL, $inputValueL, $direction[0]);
            } else {

                if ($direction[0] != $from) // если значение $direction[0] не равно значению в поле откуда, тогда пропускаем.
                    continue;
                
                $tempL = mb_strtolower ($direction[1]);
                $point = $this->autocompliteFunc($tempL, $inputValueL, $direction[1]);
                if($point !== false){
                    $deliveryPoint = $point;
                }

            }

//            if (!preg_match("/(" . $_REQUEST['volume'] . "){1}/iu", $deliveryPoint) && $_REQUEST['volume'] != '') {
//                continue;
//            }

            if ( !in_array($deliveryPoint, $list) && $deliveryPoint != false) {
                $list[] = $deliveryPoint;
            }
        }

        print json_encode($list);
        wp_die();
    }


    /*
     * Расчет сыпучих грузов
     */
    public function dryGoods() {
        /*
         *  is no termo
         */

        $getWeight = $this->GET_WEIGHT;
        $getVolume = $this->GET_VOLUME;

        /*
         * Объемный вес
         */
        $this->VOLUME_WEIGHT = $getVolume * 200;



        /*
         * проверяем стандартный груз или нет по соотношению вес\объем
         */
        if ($getWeight / $getVolume < 200 && $getVolume > 0) {
            /*
             * Стандартный груз, расчет по объему.
             */
            $matching = array(
                '9' => array(0, 1),
                '10' => array(1, 3),
                '11' => array(3, 5),
                '12' => array(5, 7),
                '13' => array(7, -1),
            );


            foreach ($matching as $key => $val) {

                $compareMin = $val[0] < $getVolume;
                $compareMax = $val[1] >= $getVolume || $val[1] == -1;

                if ($compareMin && $compareMax) {
                    $price = $this->FINDED_DIRECTION[$key];
                    $this->PRICE = $price;
                    break;
                }
            }


            $totalPrice = $getVolume * ($price * 1000);

            $minPrice = $this->FINDED_DIRECTION[2];

            if ($totalPrice < $minPrice) {
                $totalPrice = $minPrice;
            }



            $this->RES_TOTAL_PRICE = ceil($totalPrice);
            $this->RES_VOLUME = $getVolume;
            $this->RES_WEIGHT = ceil($getWeight);
            $this->RES_PALLETS = ceil($getVolume / 1.536);
        } else {
            /*             * ***************************
             * Не стандартный груз, расчет по весу.
             */

            $matching = array(
                '3' => array(0, 100),
                '4' => array(101, 200),
                '5' => array(201, 500),
                '6' => array(501, 1000),
                '7' => array(1001, 1500),
                '8' => array(1501, -1),
            );

            foreach ($matching as $key => $val) {

                $compareMin = $val[0] < $getWeight;
                $compareMax = $val[1] >= $getWeight || $val[1] == -1;

                if ($compareMin && $compareMax) {
                    $price = $this->FINDED_DIRECTION[$key];
                    $this->PRICE = $price;
                    break;
                }
            }

            $totalPrice = $getWeight * $price;

            $minPrice = $this->FINDED_DIRECTION[2];

            if ($totalPrice < $minPrice) {
                $totalPrice = $minPrice;
            }

            $this->RES_TOTAL_PRICE = ceil($totalPrice);
            $this->RES_VOLUME = $getVolume;
            $this->RES_WEIGHT = $getWeight;
            $this->RES_PALLETS = ceil($getWeight / 600);
        }
    }
   
     /*
     * Расчет палет по весу
     */
    private function getWeightPaletsCount(){
        
        $this->HALF_PALET_WEIGHT = false;
        
        if($this->GET_WEIGHT % $this->FP_WEIGHT_NORM == 0 ||
           $this->GET_WEIGHT % $this->FP_WEIGHT_NORM > $this->HP_WEIGHT_NORM){ // нет остатка, значит полные палеты
            
            $this->RES_PALLETS_WEIGHT = ceil($this->GET_WEIGHT / $this->FP_WEIGHT_NORM); // округляем до целых палет
            
        }elseif($this->GET_WEIGHT % $this->FP_WEIGHT_NORM <= $this->HP_WEIGHT_NORM){ // есть остаток равный весу половинной палеты
            
            $this->RES_PALLETS_WEIGHT = floor($this->GET_WEIGHT / $this->FP_WEIGHT_NORM) ; // Округляем до целых палет и прибавляем половинную
            $this->HALF_PALET_WEIGHT = true;
        }
        
    }
    
    /*
     * Расчет палет по объему
     */
    private function getVolumePaletsCount(){
        
        $this->HALF_PALET_VOLUME = false;
        
        if((($this->GET_VOLUME * 1000) % ($this->FP_VOLUME_NORM * 1000)) / 1000 > $this->HP_VOLUME_NORM || 
                (($this->GET_VOLUME * 1000) % ($this->FP_VOLUME_NORM * 1000)) / 1000 == 0){
            
            $this->RES_PALLETS_VOLUME = ceil($this->GET_VOLUME / $this->FP_VOLUME_NORM);
             
        }elseif((($this->GET_VOLUME * 1000) % ($this->FP_VOLUME_NORM * 1000)) / 1000 <= $this->HP_VOLUME_NORM ){
            
            $this->RES_PALLETS_VOLUME = floor($this->GET_VOLUME / $this->FP_VOLUME_NORM) ;
            $this->HALF_PALET_VOLUME = true;
        }
         
    }
    
    /*
     * Проверяем по какому значению считаем цену
     */
    private function countPaletsCompare(){
        
        self::getWeightPaletsCount();
        self::getVolumePaletsCount();
                        
        if($this->RES_PALLETS_WEIGHT < $this->RES_PALLETS_VOLUME){
            
            $this->RES_PALLETS = $this->RES_PALLETS_VOLUME;
            $this->HALF_PALET = $this->HALF_PALET_VOLUME;
            
        }else{
            
            $this->RES_PALLETS = $this->RES_PALLETS_WEIGHT;
            $this->HALF_PALET = $this->HALF_PALET_WEIGHT;
            
        }

    }
    /*
     * Термо грузы (паллеты)
     */
    public function thermoLoads() {
        $pallets == -1;
        $price == -1;
        $this->FP_WEIGHT_NORM = 600;    // Полная палета, единаца измерения - kg
        $this->HP_WEIGHT_NORM = 300;    // Половинная палета, единаца измерения - kg
        $this->FP_VOLUME_NORM = 1.536;  // Полная палета, единаца измерения - m3
        $this->HP_VOLUME_NORM = 1.280;  // Половинная палета, единаца измерения - m3
        
        self::countPaletsCompare();

        $matching = array(
            '2' => array(0, 6),
            '3' => array(6, 11),
            '4' => array(11, -1),
        );
        
        /*
         * Определили расценку по количеству палет
         */
        $pallets = $this->HALF_PALET ? $this->RES_PALLETS + 1 : $this->RES_PALLETS;
        
        foreach ($matching as $key => $val) {

            $compareMin = $val[0] <= $pallets;
            $compareMax = $val[1] >= $pallets || $val[1] == -1;

            if ($compareMin && $compareMax) {
                $this->PRICE = $this->FINDED_DIRECTION[$key];
                break;
            }
        }
        
        /*
         * Если переменные равны -1, обрываем скрипт и отдаем 500, значит в таблице нет данных*
         */
        if ($pallets == -1 || $price == -1) {
            print json_encode(['status' => 'error', 'code' => '500']);
            wp_die();
        }
        
        // Цена
        $this->RES_TOTAL_PRICE = $this->HALF_PALET ? ($this->RES_PALLETS * $this->PRICE) + ($this->PRICE * 0.7) : $this->RES_PALLETS * $this->PRICE;
        // Объём
        $this->RES_VOLUME = $this->HALF_PALET ? ($this->FP_VOLUME_NORM * $this->RES_PALLETS) + $this->HP_VOLUME_NORM : $this->FP_VOLUME_NORM * $this->RES_PALLETS ;
        // Вес
        $this->RES_WEIGHT = $this->HALF_PALET ? ($this->FP_WEIGHT_NORM * $this->RES_PALLETS) + $this->HP_WEIGHT_NORM : $this->FP_WEIGHT_NORM * $this->RES_PALLETS ;
        // количество паллет
        $this->RES_PALLETS += $this->HALF_PALET ? 0.5 : 0 ;
        
    }
     
    /*
     * Доп расчет по направлению Мск.-Питер.-Мск.
     * 
     * 
     *          ---------------------------------------------------------------------------------------------------------------------
     *    WEGHT | 0-50 | 51-100 | 101-150 | 151-200 | 201-250 | 251-300 | 301-350 | 351-400 | 401-450 | 451-500 | 501-550 | 551-600 |
     *    ---------------------------------------------------------------------------------------------------------------------------
     *    PRICE | 3100 | 3100   | 3650    | 4200    | 4750    | 5300    | 5850    | 6400    |6727     | 7225    | 7718    | 8204    | 
     *          ---------------------------------------------------------------------------------------------------------------------
     * 
     * 1м3 = 200кг
     */
    public function mskSpbMskCalc(){
        $pallets == -1;
        $price == -1;
        $this->FP_WEIGHT_NORM = 600;    // Полная палета, единаца измерения - kg
        $this->HP_WEIGHT_NORM = 300;    // Половинная палета, единаца измерения - kg
        $this->FP_VOLUME_NORM = 1.536;  // Полная палета, единаца измерения - m3
        $this->HP_VOLUME_NORM = 1.280;  // Половинная палета, единаца измерения - m3

        self::countPaletsCompare();
        
        
        $matching = array(
            '2' => array(0, 50),
            '3' => array(51, 100),
            '4' => array(101, 150),
            '5' => array(151, 200),
            '6' => array(201, 250),
            '7' => array(251, 300),
            '8' => array(301, 350),
            '9' => array(351, 400),
            '10' => array(401, 450),
            '11' => array(451, 500),
            '12' => array(501, 550),
            '13' => array(551, 600),
        );
        
        
        $rotationWeight = $this->GET_WEIGHT > 600 ? 600 : $this->GET_WEIGHT;
        
        foreach ($matching as $key => $val) {
            
            $compareMin = $val[0] <= $rotationWeight ;
            $compareMax = $val[1] >= $rotationWeight ;

            if ($compareMin && $compareMax) {
                $this->PRICE = $this->FINDED_DIRECTION[$key];
                $price = $this->PRICE;
                break;
            }
        }
        
        if ($pallets == -1 || $price == -1) {
            print json_encode(['status' => 'error', 'code' => '500']);
            wp_die();
        }

        // Объём
        $this->RES_VOLUME = $this->HALF_PALET ? ($this->RES_PALLETS * $this->FP_VOLUME_NORM) + $this->HP_VOLUME_NORM : $this->RES_PALLETS * $this->FP_VOLUME_NORM;
        // Цена
        $this->RES_TOTAL_PRICE = $this->HALF_PALET ? ($this->RES_PALLETS * $this->PRICE) + ($this->PRICE * 0.7) : $this->RES_PALLETS * $this->PRICE ;
        // количество паллет
        $this->RES_PALLETS += $this->HALF_PALET ? 0.5 : 0 ;
        // Вес
        $this->RES_WEIGHT = $this->GET_WEIGHT ;
        

    }

    /**
     * Отдает в ajax результаты расчетов стоимости 
     */
    public function calcAjaxGetResult() {

        check_ajax_referer($this->NONCE, 'nonce');

        $this->IS_TERMO = isset($_REQUEST['isTermo']) ? true : false;

        $data_list = $this->getDataList($this->IS_TERMO);

        $findedDirection = false;

        $from = mb_strtolower($_REQUEST['from-where']);
        $to = mb_strtolower($_REQUEST['to-where']);

        foreach ($data_list as $key => $direction) {

            if ($from == mb_strtolower($direction[0]) && $to == mb_strtolower($direction[1])) {
                $this->FINDED_DIRECTION = $direction;
                $findedDirection = $this->FINDED_DIRECTION;
            }
        }

        if (!$findedDirection) {
            print json_encode(['status' => 'error', 'code' => '500']);
            wp_die();
        }

        $this->GET_WEIGHT = str_replace(",", ".", $_REQUEST['weight']);
        $this->GET_VOLUME = str_replace(",", ".", $_REQUEST['volume']);

        $getWeight = $this->GET_WEIGHT;
        $getVolume = $this->GET_VOLUME;

        $weight = $this->GET_WEIGHT;
        $volume = $this->GET_VOLUME;

        /*
         * Подсчет палет
         */
        if ($this->IS_TERMO) {
            if(isset($this->FINDED_DIRECTION[0]) && 
               isset($this->FINDED_DIRECTION[1]) && 
               ($this->FINDED_DIRECTION[0] == 'Москва' || $this->FINDED_DIRECTION[0] == 'Санкт-Петербург') &&
               ($this->FINDED_DIRECTION[1] == 'Москва' || $this->FINDED_DIRECTION[1] == 'Санкт-Петербург') && 
                $this->FINDED_DIRECTION[1] != $this->FINDED_DIRECTION[0]){
                
                $this->mskSpbMskCalc();
                
            }else{
                $this->thermoLoads();
            }
            
            
        } else {
            $this->dryGoods();
        }


        print json_encode(array(
            'from' => $from,
            'to' => $to,
            'volume' => $this->RES_VOLUME,
            'weight' => $this->RES_WEIGHT,
            'numPallets' => $this->RES_PALLETS,
            'price' => $this->RES_TOTAL_PRICE
        ));
        wp_die();
    }

    public function cargoCalcShortcode() {
        require_once $this->DIR_PATCH . 'tpl/cargo-calc-viev.php';
        return;
    }

}
