jQuery(document).ready(function ($) {

// ссылка на файл AJAX  обработчик
    $ajaxDataObj = JSON.parse(ajaxDataObj);
    var ajaxurl = $ajaxDataObj.ajaxurl;
    var nonce = $ajaxDataObj.nonce;
    var nonce_ajaxFilePars = $ajaxDataObj.ajaxFilePars;

    /**
     * Загружаем список файлов для импорта
     * @returns {undefined}
     */
    function ajaxFileListLoad() {

        var $btn = $('.file_list_upload_btn');
        var $container = $('.file_list_upload_container');
        var data = new FormData();


        data.append('action', 'get_file_list');
        data.append('nonce', nonce);
        $btn.text('Загружаю список...');
        $.ajax({
            url: ajaxurl,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (respond, status, jqXHR) {
                // ОК
                if (respond.success) {
                    $btn.text('Обновить.');
//                    console.log(respond.data);
                    $container.html('');
                    $.each(respond.data, function (key, val) {

                        $container.append('<div class="cargo_calc_row_wrapp" data-dir="' + respond.data[key]['dir_path'] + '" data-name="' + respond.data[key]['file_name'] + '">' + respond.data[key]['file_name']
                                + '<span class="cargo_calc_dell" data-dell="' + respond.data[key]['dir_path'] + respond.data[key]['file_name'] + '">Удалить</span>'
                                + '<span class="cargo_calc_dwl"><a href="' + respond.data[key]['file_url'] + respond.data[key]['file_name'] + '" download="' + respond.data[key]['file_name'] + '">Скачать</a></span>'
                                + '<span class="cargo_calc_import">Загрузить в базу</span></div>');
                    });
                } else {
                    $container.text('ОШИБКА: ' + respond.error);
                }
            },
            error: function (jqXHR, status, errorThrown) {
                $container.text('ОШИБКА AJAX запроса: ' + status);
            }
        });
    }

    $('.file_list_upload_btn').on('click', function () {

        ajaxFileListLoad();
    });
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Загрузка файла в папку uploads
     * @type .files
     */
    var files; // переменная. будет содержать данные файлов
    // заполняем переменную данными, при изменении значения поля file
    $('input[type=file]').on('change', function () {
        files = this.files;
    });
    // обработка и отправка AJAX запроса при клике на кнопку upload_files
    $('.upload_files').on('click', function (event) {

        event.stopPropagation(); // остановка всех текущих JS событий
        event.preventDefault(); // остановка дефолтного события для текущего элемента - клик для <a> тега

        if (typeof files == 'undefined')
            return;
        var data = new FormData();
        $.each(files, function (key, value) {
            data.append(key, value);
        });
        data.append('action', 'ajax_fileload');
        data.append('nonce', nonce);
        var $reply = $('.ajax-reply');
        // AJAX запрос
        $reply.text('Загружаю...');
        $.ajax({
            url: ajaxurl,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            // отключаем обработку передаваемых данных, пусть передаются как есть
            processData: false,
            // отключаем установку заголовка типа запроса. Так jQuery скажет серверу что это строковой запрос
            contentType: false,
            // функция успешного ответа сервера
            success: function (respond, status, jqXHR) {
                // ОК
                if (respond.success) {
                    $reply.text('Успешно.');
                    $.each(respond.data, function (key, val) {
                        $reply.append('<p>' + val + '</p>');
                    });
                    /*
                     * Показать спписок загруженных файлов
                     */
                    ajaxFileListLoad();

                }
                // error
                else {
                    $reply.text('ОШИБКА: ' + respond.error);
                }
            },
            // функция ошибки ответа сервера
            error: function (jqXHR, status, errorThrown) {
                $reply.text('ОШИБКА AJAX запроса: ' + status);
            }

        });
    });


    ////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Удаление файлов с папки загрузок
     */
    function ajaxFileDell($this) {
        var fileLoc = $($this).attr('data-dell');
        var data = new FormData();

        data.append('action', 'ajax_dell_file');
        data.append('nonce', nonce);
        data.append('fileLoc', fileLoc);


        $container = $($this).parents('.cargo_calc_row_wrapp');

        $.ajax({
            url: ajaxurl,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (respond, status, jqXHR) {
                // ОК
                if (respond.success) {

                    console.log(respond.data);

                    $container.html('<span class="deleted">Удален.</span>');

                } else {
                    $container.text('ОШИБКА: ' + respond.error);
                }
            },
            error: function (jqXHR, status, errorThrown) {
                $container.text('ОШИБКА AJAX запроса: ' + status);
            }
        });
    }

    /**
     * Импорта данных 
     */
    function ajaxFilePars($thisFile) {

        var isTermo = false;

        if ($('.isTermo').attr("checked") == 'checked') {
            isTermo = true;
        } else {
            isTermo = false;
        }

        var fileName = $($thisFile).parents('.cargo_calc_row_wrapp').attr('data-name'); // Путь к файлу
        var data = new FormData();
        data.append('action', 'ajax_file_parse');
        data.append('nonce', nonce);
        data.append('fileName', fileName);
        data.append('isTermo', isTermo);

        $thisFile.text('Обработка...');
        $.ajax({
            url: ajaxurl,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (respond, status, jqXHR) {
                // ОК
                var printList = '';
                if (respond.success) {
                    $thisFile.text('Загрузить в базу');

                    printList += '<table style="border: 1px solid #000;"><tbody>';
//                    console.log(respond.data);
//                    $.each(respond.data, function (key, val) { //Вложеность массива изменилась в процессе написания кода...

                    $.each(respond.data, function (k, tr) {
                        if (k < 4) {
                            printList += '<tr class="not_data">';
                        } else {
                            printList += '<tr class="data">';
                        }
                        $.each(tr, function (k, td) {

                            if (td != null) {
                                printList += '<td>' + td + '</td>';
                            } else {
                                printList += '<td>&nbsp;</td>';
                            }
                        });

                        printList += '</tr>';

                    });

//                    });
                    printList += '</tbody></table>';

                    $('.cargo_calc_import_list_done').html(printList);

                } else {
                    $thisFile.text('ОШИБКА: ' + respond.error);
                }
            },
            error: function (jqXHR, status, errorThrown) {
                $thisFile.text('ОШИБКА AJAX запроса: ' + status);
            }
        });
    }


    $('.file_list_upload_container').on('click', '.cargo_calc_dell', function () {
        ajaxFileDell($(this));
    });

    $('.file_list_upload_container').on('click', '.cargo_calc_import', function () {
        ajaxFilePars($(this));
    });

});