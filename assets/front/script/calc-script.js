/* 
 * From vvg@wingi.ru page-calculation.js
 */


var deliveryCalculation = function(form, infoContainer) {
    var $form = $(form),
        infoContainer = $(infoContainer);

    if ($form.lenght < 1) return;

    var elements = {
            from: $form.find('input[name="from-where"]'),
            to: $form.find('input[name="to-where"]'),
            isTermo: $form.find('input[name="isTermo"]'),
            volume: $form.find('input[name="volume"]'),
            weight: $form.find('input[name="weight"]'),
        },
        placesElements = elements.from.add(elements.to);

//    console.log(placesElements);

    this.handlers = {};

    /**
     * Метод обрабатыает событие фокусировки на поле, где указывается конечная точка
     * @var Event event Объект события
     */
    this.handlers.placeInputFocus = function (event) {
//        console.log(event);
    }

    placesElements.on('focus', this.handlers.placeInputFocus)
};

jQuery(document).ready(function($){ 
    
    deliveryCalculation($('.calculation-form'));
    
    var $form = $('.calculation-form');
    if ($form.lenght < 1) return;

    var $from = $form.find('input[name="from-where"]'),
        $to = $form.find('input[name="to-where"]'),
        $termo = $form.find('input[name="isTermo"]');
        
        
    var listDeliveryTo = [];    
                /*
                 * Выводим списки направлений
                 * 
                 */
    var whereInputsFocusHandler = function(event){
        
        var $elem = $(event.target),
            $container = $elem.parent(),
            $tipsContainer = $container.find('.input-tip-list'),
            $tipsList,
            isTermo = $form.find('input[name="isTermo"]').prop('checked'),
            type = $elem.attr('name') == 'from-where' ? 'from' : 'to';

        $elem.data('tipSelected', false);
        $tipsContainer.detach();
        $tipsContainer = $('<div class="input-tip-list"></div>'),
        $tipsList = $('<ul></ul>');

        /*
         * Пока недоработан поиск по начальным символам искомого слова, эта проверка только мешает.
         
         if ($elem.val().length < 1 && type == 'to') {
             $tipsList.html('<li class="tip-not-click">Начните вводить название</li>');
        } else {
        */
        $tipsList.html('<li class="tip-not-click">Загрузка...</li>');
        
        
        
        requestTipsList({
                type: type,
                from: $from.val(),
                value: $elem.val(),
                isTermo: isTermo,
            }, function(list) {
                
                console.log(list);
                
                $tipsList.html('');
   
                var thisLength = list.length;
                
                if (thisLength > 0) {
                    for (var key in list) {
                        $tipsList.append('<li>'+ list[key] +'</li>');
                    }
                } else {
                    $tipsList.append('<li class="list-empty tip-not-click">Доступных городов для расчета нет. Вы можете заказть обратный звонок, что бы уточнить список доступных город.</li>');
                }
            });
//        }

        $tipsContainer.append($tipsList);
        $container.append($tipsContainer);
    }

    var tipClickHandler = function(event)
    {
        var $elem = $(event.target),
            $container = $elem.parents('.form-input'),
            $containerList = $container.find('.input-tip-list'),
            $input = $container.find('input');
        $input.val($elem.text());
        $input.data('tipSelected', true);
        $containerList.detach();
    }

    var isTermoChangeHandler = function(event)
    {
        $form.find('.input-tip-list').detach();
        $from.val('').focus();
        $to.val('');
    }

    var formSubmitHandler = function(event) {
        event.preventDefault();

        // check fields
        $form.find('input[name$="where"]').each(function(i, elem) {
            var $elem = $(elem);
            if ($elem.val().length < 1 && !$elem.data('tipSelected')) {
                if ($form.find('.input-error').length < 1) {
                    $elem.focus();
                }
                $elem.addClass('input-error');
            } else {
                $elem.removeClass('input-error');
            }
        });

        var $weight = $form.find('input[name="weight"]'),
            $value = $form.find('input[name="volume"]'),
            weightIsEmpty = $weight.val().length < 1,
            valueIsEmpty = $value.val().length < 1;

        if (weightIsEmpty && valueIsEmpty) {
            $weight.addClass('input-error');
            if ($form.find('.input-error').length < 1) {
                $weight.focus();
            }
            $value.addClass('input-error');
        } else {
            $weight.removeClass('input-error');
            $value.removeClass('input-error');
        }

        if ($form.find('.input-error').length < 1) {
            var data = {},
                formData = $form.serializeArray();
            for (var key in formData) {
                data[formData[key].name] = formData[key].value;
            }

            data = $.extend({
                nonce: ajaxFrontData.nonce,
                action: 'calc-result'
            }, data);

            $.ajax({
                method: 'POST',
                url: ajaxFrontData.ajax,
                data: data,
                success: function(response)
                {
                    response = JSON.parse(response);
                    $form.parent();
                    var $container = $('.calculation-result'),
                        $volume = $('<div class="info-volume">Объем: ' + response.volume + ' м<sup>3</sup></div>'),
                        $weight = $('<div class="info-weight">Вес груза: ' + response.weight + ' кг</sup></div>'),
                        $numPallets = $('<div class="info-numPallets">Паллет: ' + response.numPallets + '</sup></div>'),
                        $price = $('<div class="info-price">Время в пути: ' + response.price + ' руб.</sup></div>');

                    $container.add('.culculation-send-form').css('display', 'block');
                    $container.find('.volume-text').text(response.volume);
                    $container.find('.weight-text').text(response.weight);
                    $container.find('.pallets-text').text(response.numPallets);
                    $container.find('.price-text').text(response.price);
                    renderMap();
                }
            });
        }
    }


/*
 * Обработка полей формы по событию onChange
 */
    var requestTipsList = function (data, successFunction)
    {
        var xhrName = 'xhr' + data.type; 
        
        

        data = $.extend({
            nonce: ajaxFrontData.nonce,
            action: 'calc-get-palces'
        }, data);
        
        //console.dir(data);

        if (ajaxFrontData[xhrName]) ajaxFrontData[xhrName].abort();
        
        ajaxFrontData[xhrName] = $.ajax({
            method  : 'POST',
            url     : ajaxFrontData.ajax,
            data    : data,
            cache: false,
            success : function(response)
            {

                response = JSON.parse(response);
//                console.log(response);
                successFunction(response);
            }
        });
    }

    var renderMap = function()
    {
        var isLoaded = $('[data-ymaps-loaded]').length < 1,
            apiMapSrc = "https://api-maps.yandex.ru/2.1/?lang=ru_RU&ver=2.1&onload=mapInit";

        if (isLoaded) {
            window.mapInit = function() { renderMap(); }
            $.getScript(apiMapSrc, function() {
                $('body').attr('data-ymaps-loaded', 1);
            });
        } else {
            var from = $from.val(),
                to = $to.val();

            if (typeof this._map == 'undefined') {
                this._map = new ymaps.Map('map', {
                    center: [55.751574, 37.573856],
                    zoom: 9
                });
            } else {
                this._map.geoObjects.removeAll();
            }

            routeModel = new ymaps.multiRouter.MultiRouteModel([
                from,
                to
            ], {
                wayPointDraggable: false,
                boundsAutoApply: true,
                results: 1,
            });

            routeObject = new ymaps.multiRouter.MultiRoute(routeModel, {
                wayPointDraggable: false,
                boundsAutoApply: true,
            });

            routeModel.events.once('requestsuccess', function()
            {
                var route = routeObject.getRoutes().get(0),
                    $container = $('.calculation-result');
                $container.find('.destanation-text').text(route.properties.get("distance").text);
                $container.find('.duration-text').text(route.properties.get("duration").text);
            });

            this._map.geoObjects.add(routeObject);

            this._map.controls.remove('miniMap');
            this._map.controls.remove('typeSelector');
            this._map.controls.remove('scaleLine');
            this._map.controls.remove('searchControl');
            this._map.controls.remove('trafficControl');
            this._map.controls.remove('mapTools');
            this._map.controls.remove('geolocationControl');
        }
    }
 
    
    $from.on('focus', whereInputsFocusHandler);
    $from.on('keyup', whereInputsFocusHandler);
    $from.parent().on('click', '.input-tip-list ul li:not(".tip-not-click")', tipClickHandler);
    
    $to.on('focus', whereInputsFocusHandler);
    $to.on('keyup', whereInputsFocusHandler);
    $to.parent().on('click', '.input-tip-list ul li:not(".tip-not-click")', tipClickHandler);
    $termo.on('change', isTermoChangeHandler);

    $form.on('submit', formSubmitHandler);

    $('.calculation-result .map-highlite').on('click', function(event)
    {
        var target = $(event.target),
            container = target.parent();
        container.toggleClass('open');
        window._map.container.fitToViewport();
    });
});