<?php

/*
  Plugin Name: Cargo Calc
  Description: Калькулятор стоимости доставки для "Евро-транс".
  Version: 1.0
  Author: yav@wingi.ru
  Author URI: http://web-lab.men
  Copyright: wingi.ru

 */

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

require_once plugin_dir_path(__FILE__) . 'modules/CargoCalc.php';

if (!class_exists('WingiCargoCalc')):

    class WingiCargoCalc
    {

        private $OPTION_NAME,
                $DIR_PATCH,
                $DIR_PATCH_UPLOADS,
                $URL_PATCH,
                $URL_PATCH_UPLOADS,
                $CARGO_CALC_OBJ;

        public function __construct() {

            $this->OPTION_NAME = get_class($this) . '_settings';
            $this->DIR_PATCH = plugin_dir_path(__FILE__);
            $this->DIR_PATCH_UPLOADS = $this->DIR_PATCH . 'uploads/';
            $this->URL_PATCH = plugin_dir_url(__FILE__);
            $this->URL_PATCH_UPLOADS = $this->URL_PATCH . 'uploads/';

            $this->CARGO_CALC_OBJ = new modules\CargoCalc();
            $this->CARGO_CALC_OBJ->setBaseDirPath($this->DIR_PATCH);
            $this->CARGO_CALC_OBJ->setOptionName($this->OPTION_NAME);


            add_action('admin_menu', array($this, 'addSettingsPage'));

            add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));

            add_action('init', array($this, 'enqueue_adnin_scripts'));

            add_action('wp_ajax_ajax_fileload', array($this, 'upload_callback'));

            add_action('wp_ajax_get_file_list', array($this, 'get_file_list'));

            add_action('wp_ajax_ajax_file_parse', array($this, 'ajax_file_parse'));

            add_action('wp_ajax_ajax_dell_file', array($this, 'ajax_dell_file'));
            
        }

        public function addSettingsPage() {

            // Регистрируем пункт меню плагина
            add_menu_page(
                    'Общие настройки',
                    'Cargo Calc',
                    'manage_options',
                    'wingi-price-calc',
                    array($this, 'settingsPage'),
                    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAMAAAC6V+0/AAABm1BMVEVHcEzIeAcYEQYBAQH9mQvskArJdwbchQr7mAs6IwLUgQnzlAuysrK2trfIyMjBdAepqamwZwfzoAz9ngtzc3P4mAv+qgx7e3vTgAnmjAttbW3YgwrvkQvPfQmXWwcoFgKIiIi0bgi9vb3KysqUYAf+ngvlkwqnbAjNzc3JgQnQfQmyucS+fSPvjgWorbLIzdPEwLrliQeloZz0lAr8mQv6mAv7kQL2lgv+kwD6lAL7jwD9lgL0kgfd3d3ykwv9nA39kQT8lAj6tlT/mwrvnyza2trg6ff6iQD6pCf4lwv7rkD4jgDm5+f/nAzs7O3/nAbf4OL6mhH+oAv9mwz70ZP7zIr+mwv84Lj/pwz////6mQ36nhn6nBD/lwL6pzSVlZW1tbXpmyra3+Xw8fP/lwnj7/3Ly8vj6fH6lwfY1M36oSX7mAn858n6tmHsjwr/sQ398+X4nzT5gADwkAbwoS7j4+Pe29Xc19H7sUbo7fTZ1c7/xA/83K7Hx8fT09OkpKS6urro6erAwMCjo6Pp7vXb5PH0oi3U0Mnd4urUGMSTAAAAM3RSTlMAjBkLnvWKkp41k56Ki/SNjB3kgYX69nPTl36Smy92KHS2kuCUi+h89YyOiov0jPT09Iw6FgtYAAABP0lEQVQY03XR1XLCQBSAYbRFCqXu7i4bAtkkhYSmaKC4Ffe6l7rLY3cTuOW/2J35ZvZcnJVIWjQ2p1KpFFqtAp09XQ1T9weNRiMGAIauQLdMtL5YAhKJJjrPBtUINQHskk3CIAAW3G6HvK0T4RCGn5xHD60AWF2hEGs1LSNsx5iL47CfAYBxsGy6CNpExKDDTwgzCQgJvoG2iMVMBnmT2yyUcguoAWAHBU5z17uon9wqwhGn04QirvKlPVTpe0WcacFxeJSibivvhYLXs68TEa87XAckdXf/ks2WvXRHA9PxJztJPXw9ZzJvXk7A0QisR8MMSdUq5c/XDw83jXA8Vky6q+j5b56jae6GHkAon3r02Xy8r1r7o2naE++Vi7ubnVcqlWtbmxt6/eLMcHPJEwtLUqm0c3vdYNBNylp9xT/K/ksQtAbB1AAAAABJRU5ErkJggg==',
                    90
            );

            // Регистрируем страницу плагина
            $page = add_submenu_page(
                    'wingi-price-calc', // Родительская страница меню
                    'Импорт параметров', // Название пункта меню
                    'Импорт', // Заголовок страницы
                    'manage_options', // Возможность, определяющая уровень доступа к пункту
                    'wingi-price-calc-settings', // Ярлык (часть адреса) страницы плагина
                    array($this, 'settingsPageImport')  // Функция, которая выводит страницу
            );

            // Используем зарегистрированную страницу для загрузки скрипта
            add_action('admin_print_scripts-' . $page, 'enqueue_adnin_scripts');
        }

        /**
         * Получаем данные из таблицы опций 
         * @return array
         */
        public function getOptoons() {
            $option = get_option($this->OPTION_NAME);

            if (!empty($option)) {

                $option = unserialize($option);

                return $option;
            } else {
                return FALSE;
            }
        }

        public function settingsPage() {
            echo '<h1>Страница в разработке</h1>';
            ?>
            <hr>
            <h2> Info Block : </h2>
            <p></p>
            <p>Шорткод для вывода калькулятора [cargo_calc]</p>
            <p>Фильтр для подмены формы "cargo_calc_form"</p>
            <p>Страница для загрузки данных с xlsx <a href="/wp-admin/admin.php?page=wingi-price-calc-settings">Стр. импорта данных</a></p>

            <?php
        }

        public function settingsPageImport() {
            ?>

            <input type="file" multiple="multiple" />
            <button class="upload_files">Загрузить файл</button>
            <hr>

            <div class="ajax-reply"></div>
            <div class='import_file_list'>

                <div class='file_list_upload'>
                    <label>Термо <input class="isTermo" type="checkbox" name="isTermo" value="1"/></label><br>
                    Cписок файлов для импорта данных <span class='file_list_upload_btn'>Показать список</span>
                    <hr>
                </div>
                <div class='some_file_list'>
                    <div class="file_list_upload_container">

                    </div>
                </div>
            </div>
            <hr>
            <div class="cargo_calc_import_list_done"></div>
            <?php

        }

        /*
         * Удаление файла с папки загрузки.
         */
        public function ajax_dell_file() {

            check_ajax_referer('uplfile', 'nonce');

            $fileLoc = $_POST['fileLoc'];

            if (file_exists($fileLoc)) {
                unset($_POST['fileLoc']);
                unlink($fileLoc);
            }

            wp_send_json_success();
        }

        /**
         * Парсим xlsx по ajax запросу.
         */
        public function ajax_file_parse() {
            check_ajax_referer('uplfile', 'nonce');

            if (!isset($_POST['fileName']) || empty($_POST['fileName']) || !isset($_POST['isTermo']))
                die();

//            $res_array = array();

            $isTermo = $_POST['isTermo'];

            require_once $this->DIR_PATCH . 'Classes/PHPExcel.php' ;

            $filName = $_POST['fileName'];
            $filePath = $this->DIR_PATCH_UPLOADS . $filName;

            $type = PHPExcel_IOFactory::identify($filePath);
            $objReader = PHPExcel_IOFactory::createReader($type);
            $objPHPExcel = $objReader->load($filePath);

            $sesArray = array();
            $worksheets = array();
            $i = 0;


            /*
             * Убираем запятые.
             */
            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                $worksheets = $worksheet->toArray();
                foreach ($worksheets as $numberRow => $row) {

                    if ($numberRow <= 3)
                        continue;

                    foreach ($row as $numberCell => $cell) {
                        $sesArray[$i][$numberRow][$numberCell] = preg_replace('~^(\d*)?(?:,){1}(\d*)?$~m', "\$1.\$2", $cell);
                    }
                }
                $i++;
            }

            $termo = $isTermo == 'true' ? 'isTermo' : 'noTermo';

            $opt = $this->getOptoons();
            $opt[$termo] = $sesArray[0];

            /*
             * сохраняем данные
             */
            update_option($this->OPTION_NAME, serialize($opt));

            /*
             * Отправляем массив в ajax 
             */
            wp_send_json_success($opt[$termo]);
        }

        /**
         * Получаем список загруженных файлов 
         */
        public function get_file_list() {

            check_ajax_referer('uplfile', 'nonce'); // защита

            $resp = array();
            $files1 = scandir($this->DIR_PATCH_UPLOADS);

            /**
             * Удаляем точки
             */
            if (is_array($files1)) {

                $dot = array_search('.', $files1);
                if ($dot === 0 || $dot) {
                    unset($files1[$dot]);
                }

                $dubldot = array_search('..', $files1);
                if ($dubldot) {
                    unset($files1[$dubldot]);
                }
            }
            $i = 0;
            foreach ($files1 as $k => $v) {
                $resp[$i]['dir_path'] = $this->DIR_PATCH_UPLOADS;
                $resp[$i]['file_name'] = $v;
                $resp[$i]['file_url'] = $this->URL_PATCH_UPLOADS;
                $i++;
            }

            wp_send_json_success($resp);
        }

        /**
         * Обработка ajax загрузки файла
         */
        public function upload_callback() {

            check_ajax_referer('uplfile', 'nonce'); // защита

            if (empty($_FILES))
                wp_send_json_error('Файлов нет...');

            $resp = array();

            $is_xlsx = false;

            foreach ($_FILES as $file_id => $data) {

                $fileName = $data['name'];

                $is_xlsx = preg_match('~^(?:.*)+\.xls[x]?$~si', $fileName);

                $uploadfile = $this->DIR_PATCH_UPLOADS . basename($fileName);

                if ($is_xlsx) {
                    if (move_uploaded_file($data['tmp_name'], $uploadfile)) {
                        $resp[] = "{$fileName} - загружен. \n";
                    } else {
                        $resp[] = "{$fileName} - не загружен. \n";
                    }
                } else {
                    $resp[] = "Не подходящий формат - {$fileName},\n загрузите файл в формате .xlsx или .xls \n";
                }
            }

            wp_send_json_success($resp);
        }

        /**
         * Перадаем данные в скрипт
         */
        public function localize_script_optoons() {

            $opt['ajaxurl'] = admin_url('admin-ajax.php');
            $opt['nonce'] = wp_create_nonce('uplfile');

            return json_encode($opt);
        }

        /**
         * подключаем скрипты к админке сайта
         */
        public function enqueue_adnin_scripts() {
            if (is_admin()):


                wp_register_script('cargo-calc-js', plugins_url('assets/admin/script/calc-script.js', __FILE__));
                wp_enqueue_script('cargo-calc-js');

                $opt = $this->localize_script_optoons();
                wp_localize_script('cargo-calc-js', 'ajaxDataObj', $opt);

                wp_register_style('calc-style-css', plugins_url('assets/admin/style/calc-style.css', __FILE__));
                wp_enqueue_style('calc-style-css');

            endif;
        }

        /**
         * подключаем скрипты к внешней части сайта
         */
        public function enqueue_scripts() {
//            if($_SERVER['REMOTE_ADDR'] === '188.163.47.30'){
            wp_register_script('cargo-calc-js', plugins_url('assets/front/script/calc-script.js', __FILE__), array('jquery', 'jquery-ui.min-js'), '', true);
            wp_enqueue_script('cargo-calc-js');
            
            wp_register_script('jquery-ui.min-js', plugins_url('assets/front/script/jquery-ui.min.js', __FILE__), array(), '', true);
            wp_enqueue_script('jquery-ui.min-js');

            $this->CARGO_CALC_OBJ->frontEndLocalizeScript();

            wp_register_style('calc-style-css', plugins_url('assets/front/style/calc-style.css', __FILE__));
            wp_enqueue_style('calc-style-css');
//            }
        }

    }

    $wingiCargoCalc = new WingiCargoCalc();

else:

    error_log('Класс WingiCargoCalc, уже объявлен');
    
endif;

